# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 14:26:46 2020

@author: Ulla
"""
import numpy as np
import pandas as pd
import glob
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from tqdm import tqdm
#-------------------------------------------------------------------------------------------------------------
#input: csv-file originated from ImageJ. The columns contain Distance_(um) is the lenght of the linescan, Value is the background intensity, Y1 is the pre-bleach intensity, Y2 to Yn is the mean intensity along the stack
#the function substract the background from each intensity and scales all values by the pre-bleach intenstity
def bgsubtraction(df):
    df_x=df["Distance_(um)"]
    df_before=df["Y1"]
    df_bg=df["Value"]
    #Background subtraction
    tmp=df.sub(df_bg, axis=0)
    tmp=tmp.drop(["Distance_(um)", "Value"], axis=1)
    #divide every column by the intensity before bleach
    df_relativ=tmp.div(tmp["Y1"], axis=0)
    df_relativ=df_relativ.reindex(sorted(df_relativ.columns, key=lambda x: int(x.split('Y')[1])), axis=1)    
    #create final table
    result = pd.concat([df_x, df_relativ ], axis=1, sort=False)
    return result
#---------------------------------------------------------------------------------------------------------------
#the functions fit the FRAP curve and caluclates all parameters
def tofit(t,maxInt,tau):
    return (maxInt*(1-np.exp(-t*tau)))

def generateFit(df,path):
        sns.set_palette("tab10")
        
        #w is the lenght of the linescan; Distance_(um) and Y1 are excluded from the fit
        #w=(df["Distance_(um)"].max())/10**4
        df=df.drop(["Distance_(um)", "Y1"], axis=1)
        
        #y_fit contains the median intensity for each column, means the median intensity of a linescan per image
        #t_fit contains a list with the time for each image (framerate: 2 images/sec)
        y_fit=np.asarray(df.median())
        t_fit=np.arange(0.5,len(df.columns)/2+0.5,0.5)
        
        #Figure x-axis labeling
        label=[ "" if x%15 else str(x/2) for x in range(len(df.columns)) ]
        label[-1]=str(t_fit[-1])
        
        #actual curve fit
        popt, pcov = curve_fit(tofit, t_fit, y_fit-y_fit[0])
        maxInt,tau=popt
        
        fitD=np.asarray([tofit(x,maxInt,tau) for x in t_fit])+y_fit[0]
        
        #calculate the intensity directly after the bleach (I0) and the mobile Fraction (mobFrac)
        I0=np.min(fitD)
        mobFrac=((maxInt-I0)/(1-I0))
        
        #plot the FRAP curve including the fit for each stack from one patch and save one image
        plt.figure()
        ax1 = sns.boxplot(data=df, color="white", zorder=0)
        for i,box in enumerate(ax1.artists):
            box.set_edgecolor('darkgrey')
            box.set_facecolor('white')
            for j in range(i*6,i*6+6):
                line = ax1.lines[j]
                line.set_color('darkgrey')
                line.set_mfc('darkgrey')
                line.set_mec('darkgrey')
        ax1.set_xlabel("Time",fontsize=15)
        ax1.set_xticklabels(label)
        ax1.set_ylabel("rel.Intensity",fontsize=15)
        ax1.tick_params(labelsize=15)
        ax1.plot(fitD, color="darkred", linewidth=3, zorder=1)
        plt.savefig(path, bbox_inches="tight")
        plt.close()         
        #calculate halflife time (tau_half) and the immobile Fraction (immobFrac)
        tau_half=np.log(0.5)/-tau
        immobFrac=1-mobFrac
        
        return tau, tau_half ,fitD, maxInt, I0, mobFrac,immobFrac
#---------------------------------------------------------------------------------------------------------------  
# the function is relevant for opening of files. It manages different seperators of the input file (, or ;)
def tryOpen(path):
    df=pd.read_csv(path, sep=";")
    if(len(df.columns)>1):
        return df
    else:
        return pd.read_csv(path, sep=",")
    
#--------------------------------------------------------------------------------------------------------------
#actual programm:
        
#reads all folders * in the folder "FRAP"
pathIN=r"C:\\Users\\Ulla\\Desktop\\BA\\Daten\\FRAP\\*\\"

# input options: "_in", "_ring1", "_out1"
pos="_out1"

#reads all csv-files for the given path
allFiles=glob.glob(pathIN+"*\\*\\*"+pos+"*.csv")

#create empty arrays for all output parameter
allTau=[]
allFits=[]
allMaxInt=[]
allI0=[]
allTau_half=[]
allmobFrac=[]
allimFrac=[]
#allD=[]

#iterate through all files and perform the background substraction (with replacing values <0 by NaN), save the corrected output files and
#calculates the output parameters
for file in allFiles:
    if(file.split("\\")[-1].split("_")[-1]=='corrected.csv'):
        continue
    print("Running on:", file)
    if(pos=="_in"):
        df3=tryOpen(file)
        #df3=pd.read_csv(file, sep=";")
    else:
        file2=file[:-5]+"2.csv"
        df1=tryOpen(file)
        df2=tryOpen(file2)
#        df1=pd.read_csv(file, sep=";")
#        df2=pd.read_csv(file2, sep=";")
        df3=df1.append(df2)
    df3=bgsubtraction(df3)
    df3[df3<0]=np.nan
    splitted=file.split("\\")
    splitted[-1]=("_").join(splitted[-1].split("_")[0:2])+pos+"_corrected.csv"
    outPath=("\\").join(splitted)
    df3.to_csv(outPath, sep=";")
    tau, tau_half ,fitD, maxInt, I0, mobFrac,imFrac=generateFit(df3, outPath[:-3]+"pdf")
    allTau.append(tau)
    allFits.append(fitD)
    allMaxInt.append(maxInt)
    allI0.append(I0)
    allTau_half.append(tau_half)
    allmobFrac.append(mobFrac)
    allimFrac.append(imFrac)
    #allD.append(D)
      
tmp = pd.DataFrame(allFits).melt()

#plot of all fits and theire standard deviation and save them as pdf file
plt.figure()
sns.set_style("whitegrid")
sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 1.5})
ax=sns.lineplot(x="variable", y="value",color="darkgray", data=tmp, ci="sd")
ax=sns.lineplot(x="variable", y="value",color="darkred", data=tmp, ci=None)
label2=np.arange(0, 40, 7.5).tolist()
ax.set(ylim=(0, 1.2))
ax.set_xticklabels(label2)
ax.set(xlim=(0, 50))
plt.savefig(r"C:\Users\Ulla\Desktop\BA\Daten\FRAP\fits"+pos+".pdf", bbox_inches="tight")

#summarize all output parameter in one table and save them as csv file
df_results=pd.DataFrame()
df_results["Tau"]=allTau
df_results["I0"]=allI0
df_results["MaxInt"]=allMaxInt
df_results["Tau_half"]=allTau_half
df_results["mobileFrac"]=allmobFrac
df_results["imFrac"]=allimFrac
#df_results["D"]=allD

df_results.to_csv(r"C:\Users\Ulla\Desktop\BA\Daten\FRAP\results"+pos+".csv", sep=';')
                
                
