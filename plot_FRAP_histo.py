# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 14:55:54 2020

@author: Ulla
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import iqr

# %%
#insert path to load csv-file and obtain a dataframe
#the csv-file includes values of all evaluated FRAP measurements obtained from Python Script "calculation_FRAP.py"
dataIN = r"C:\Users\Ulla\Desktop\BA\Daten\results_FRAP\py_results_immobFrac.csv"
df = pd.read_csv(dataIN, sep=";")
caseName = dataIN.split("/")[-1].split(".")[0]
sns.set_style("whitegrid")

# %%
#calculation of binwidth according the Freedman–Diaconis rule for the different regions
tmp1=iqr(df,axis=0,nan_policy='omit')
bin_out=(2*(tmp1[0]/pow(len(df["outer region"]),1/3)))
bin_in=2*(tmp1[1]/pow(len(df["inner region"]),1/3))
bin_ring=2*(tmp1[2]/pow(len(df["ring"]),1/3))

bin_out=int(np.ceil((df["outer region"].max()-df["outer region"].min())/bin_out))
bin_in=int(np.ceil((df["inner region"].max()-df["inner region"].min())/bin_in))
bin_ring=int(np.ceil((df["ring"].max()-df["ring"].min())/bin_ring))

#plot of histogram for each region mit manually set color options
plt.figure()
ax1=sns.distplot(df["outer region"],bins=bin_out, kde_kws={"color":"C0"}, hist_kws={"rwidth":0.9, 'alpha':1.0, "color":"darkgrey"})
ax1.set(ylim=(0,5))
ax1=plt.grid(b=True, linestyle='-', alpha=0.2)
plt.show()
plt.savefig("histplot_FRAP_imFrac_out.pdf", bbox_inches="tight")

ax2=sns.distplot(df["inner region"],bins=bin_in, kde_kws={"color":"C1"}, hist_kws={"rwidth":0.9, 'alpha':1.0, "color":"darkgrey"})
ax2.set(ylim=(0,5))
ax2=plt.grid(b=True, linestyle='-', alpha=0.2)
plt.show()
plt.savefig("histplot_FRAP_imFrac_in.pdf", bbox_inches="tight")

ax3=sns.distplot(df["ring"],bins=bin_ring, kde_kws={"color":"C2"}, hist_kws={"rwidth":0.9, 'alpha':1.0, "color":"darkgrey"})
ax3.set(ylim=(0,5))
ax3=plt.grid(b=True, linestyle='-', alpha=0.2)
plt.show()
plt.savefig("histplot_FRAP_imFrac_ring.pdf", bbox_inches="tight")

# %%
#insert column name of the data frame to obtain the median, mean and standard deviation of the case
#insert options: outer region, inner region, ring
name="outer region"
print("median:",np.nanmedian(df[name]))
print("mean:",np.mean(df[name]))
print("STD:",np.std(df[name]))

