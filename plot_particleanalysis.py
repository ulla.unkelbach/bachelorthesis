# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 17:10:20 2020

@author: Ulla
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# %%
#insert path to load csv-file and obtain a dataframe
#the csv-file includes values of all evaluated pictures obtained from Fiji particle analysis 
dataIN = r"C:\Users\Ulla\Desktop\BA\Daten\results_particelanalysis\py_results.csv"
df = pd.read_csv(dataIN, sep=";")
caseName = dataIN.split("/")[-1].split(".")[0]

# %%
#edit the dataframe by removing the irrelevant columns, only the intensities/area for all conditions (wildtype HEK, transfected and induced HEK, wafer) remain
tmp=df.drop(columns=['wt1', 'wt2','wt4','wt5','cav1','cav2','cav4','cav5','w1','w2','block_wt1', 'block_wt2','block_wt4','block_wt5','block_cav1','block_cav2','block_cav4','block_cav5','block_w1','block_w2'])

# %%
#definition of plot settings and colors
a4_dims = (11.7, 12)
fig, ax = plt.subplots(figsize=a4_dims)
sns.set(font_scale=2)
sns.set_style("whitegrid")
my_pal = {"wt3":"tab:blue", "wt6":"cornflowerblue", "cav3":"orangered","cav6":"coral","w3":"grey","block_wt3":"tab:blue","block_wt6":"cornflowerblue","block_cav3":"orangered","block_cav6":"coral","block_w3":"grey"}

#boxplot of dataframe and save image as pdf
ax1=sns.boxplot(data=tmp, palette=my_pal)
ax1.set_ylabel("Intensity/area")
ax1.set_xticklabels(['WT patch','WT in', 'Cav patch', 'Cav in', 'wafer', 'block WT patch',' block WT in', 'block Cav patch', 'block Cav in', 'block wafer'],rotation=90)

plt.savefig("boxplot_part.analysis.pdf", bbox_inches="tight")

# %%
#insert column name of the data frame to obtain the median, Q3, Q1 and IQR as well as mean and standard deviation of the case
#insert options: wt3, wt6, cav3, cav6, w3, block_wt3, block_wt6, block_cav3, block_cav6, block_w3
name="block_w3"
print("median:",np.nanmedian(df[name]))
q75,q25=np.nanpercentile(df[name],[75,25])
print("Q3:",q75)
print("Q1:",q25)
print("IQR:",q75-q25)
print("mean:",np.mean(df[name]))
print("STD:",np.std(df[name]))


