# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 16:55:45 2020

@author: Ulla
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

# %%
#insert path to load csv-file and obtain a dataframe
#the csv-file includes values of all evaluated pictures obtained from Fiji simFRAP plugin analysis 
dataIN = r"C:\Users\Ulla\Desktop\BA\Daten\simFRAP\results_simfrap_total.csv"
df = pd.read_csv(dataIN, sep=";")
caseName = dataIN.split("/")[-1].split(".")[0]
sns.set_palette("tab10")

# %%
#boxplot of dataframe and save image as pdf
ax1 = sns.boxplot(data=df)
ax1.set_xlabel("Case",fontsize=15)
ax1.set_ylabel("Diffusion coefficient",fontsize=15)
ax1.tick_params(labelsize=15)

plt.savefig("boxplot_simFRAP.pdf", bbox_inches="tight")

# %%
#insert column name of the data frame to obtain the median, Q3, Q1 and IQR as well as mean and standard deviation of the case
#insert options: outer region, inner region, ring
name="outer region"
print("median:",np.nanmedian(df[name]))
q75,q25=np.nanpercentile(df[name],[75,25])
print("Q3:",q75)
print("Q1:",q25)
print("IQR:",q75-q25)
print("mean:",np.mean(df[name]))
print("STD:",np.std(df[name]))



