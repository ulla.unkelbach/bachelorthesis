# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 14:55:54 2020

@author: Ulla
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# %%
#insert path to load csv-file and obtain a dataframe
#the csv-file includes values of all evaluated FRAP measurements obtained from Python Script "calculation_FRAP.py"
dataIN = r"C:\Users\Ulla\Desktop\BA\Daten\results_FRAP\py_results_immobFrac.csv"
df = pd.read_csv(dataIN, sep=";")
caseName = dataIN.split("/")[-1].split(".")[0]

# %%
#boxplot of dataframe (immobile fractions) and save image as pdf
ax = sns.boxplot(data=df)
ax.set(ylim=(0, 1))
ax.set_xlabel("Case",fontsize=15)
ax.set_ylabel("immobile Fraction",fontsize=15)
ax.tick_params(labelsize=15)
sns.set_style("whitegrid")

plt.savefig("boxplot_FRAP_imFrac.pdf", bbox_inches="tight")

# %%
#insert column name of the data frame to obtain the median, Q3, Q1 and IQR as well as mean and standard deviation of the case
#insert options: outer region, inner region, ring
name="outer region"
print("median:",np.nanmedian(df[name]))
q75,q25=np.nanpercentile(df[name],[75,25])
print("Q3:",q75)
print("Q1:",q25)
print("IQR:",q75-q25)
print("mean:",np.mean(df[name]))
print("STD:",np.std(df[name]))





