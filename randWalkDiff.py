# %%
import numpy as np
import matplotlib.pyplot as plt
import math
import random
from tqdm import tqdm


def UniformRandomPointInCircle(inputRadius=1, xcenter=0, ycenter=0, n=1000):
    x = np.zeros(n)
    y = np.zeros(n)
    for i in range(n):
        r = inputRadius * math.sqrt(random.random())
        theta = 2 * math.pi * random.random()
        x[i] = xcenter + r*math.cos(theta)
        y[i] = ycenter + r*math.sin(theta)
    return x, y


def randWalk(x, y, dims=2, ):
    step_set = np.asarray([-1, 0, 1])/10
    # origin = np.zeros((1, dims))
    step_shape = (x.shape[0], dims)
    steps = np.random.choice(a=step_set, size=step_shape)
    x_new = x+steps[:, 0]
    y_new = y+steps[:, 1]
    return x_new, y_new


# %%
x1 = np.random.uniform(-1, 1, int(1000))
y1 = np.random.uniform(0, 0.01, int(1000))
x2, y2 = UniformRandomPointInCircle()


# %%

for t in tqdm(range(1000)):
    f, axs = plt.subplots(1, 2, sharey=True)

    axs[0].scatter(x1, y1)
    axs[1].scatter(x2, y2)

    for ax in axs:
        ax.axis('equal')
        ax.set_xlim([-15, 15])
        ax.set_ylim([-15, 15])

    plt.savefig("./out/rndWalk_"+str(t).zfill(5)+".png")
    plt.close()
    x1, y1 = randWalk(x1, y1)
    x2, y2 = randWalk(x2, y2)


# %%
